require "sinatra"
require "rqrcode"

set :bind, '0.0.0.0'

get "/" do
  erb :index
end

post "/" do
  url = params["url"]
  image_size = params["image_size"]
  qrcode = RQRCode::QRCode.new(url)

  puts params.inspect
  size = case image_size
    when "small"
      100
    when "medium"
      200
    when "large"
      400
    else
      puts "Unknown image_size: #{image_size.inspect}"
      200
    end

  # NOTE: showing with default options specified explicitly
  png = qrcode.as_png(
    bit_depth: 1,
    border_modules: 4,
    color_mode: ChunkyPNG::COLOR_GRAYSCALE,
    color: "black",
    file: nil,
    fill: "white",
    module_px_size: 6,
    resize_exactly_to: false,
    resize_gte_to: false,
    size: size
  )

  content_type 'image/png'
  png.to_blob
end

