# Docker

## Developing

- Edit files
- Rebuild container: `docker build --tag lisa-codes .`
- Run locally: `docker run -p 80:4567 lisa-codes`

### Run a command in a container from the image

- `docker build --tag lisa-codes . && docker run --rm --name lisa-codes-web lisa-codes ls -ltr`
- `docker build --tag lisa-codes . && docker run -p 80:4567 --rm --name lisa-codes-web lisa-codes

## Deploying

This depends on the existance of `heroku.yml`.  

Given these remotes:

```
heroku  https://git.heroku.com/lisa-codes.git (fetch)
heroku  https://git.heroku.com/lisa-codes.git (push)
origin  git@gitlab.com:CraigTreptow/lisa-codes.git (fetch)
origin  git@gitlab.com:CraigTreptow/lisa-codes.git (push)
```
We can now git push to Heroku and when Heroku sees the `heroku.yml` file, it will build a Docker image from the Dockerfile and then start the container.

So, deploying becomes:

1. Commit changes as normal and push to repo: `git push origin`
1. deploy to heroku: `git push heroku`
