FROM ruby:3.2

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

WORKDIR /code

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

# Tell Docker to listen on port 4567.
EXPOSE "4567"

# Tell Docker that when we run "docker run", we want it to
# run the following command:
# $ bundle exec rackup --host 0.0.0.0 -p 4567.
# CMD ["bundle", "exec", "rackup", "--host", "0.0.0.0", "-p", "4567"]
CMD ["bundle", "exec", "ruby", "lisa-codes.rb", "-s", "Puma"]
